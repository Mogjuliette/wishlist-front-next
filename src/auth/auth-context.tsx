import { User } from "@/entities";
import { destroyCookie, parseCookies, setCookie } from "nookies";
import { createContext, useEffect, useState } from "react";

interface AuthState {
    token?: string | null;
    setToken: (value: string | null) => void;
    user?: User | null;
    setUser: (value: User | null) => void;
}

/**Dans ce fichier, on définit notre token et notre user connecté, 
 * et grâce à react context, on pourra y accéder depuis n'importe 
 * quelle page sans la faire passer de page en page sous forme de props */
export const AuthContext = createContext({} as AuthState);

export const AuthContextProvider = ({ children }: any) => {
    const [token, setToken] = useState<string | null>('');
    const [user, setUser] = useState<User | null>();
 
    /**Dans cette fonction, on définie ce qui se passe quand on change la valeur du token */
    function handleSet(value: string | null) {
        if (value) {
            setCookie(null, 'token', value)
        } else {
            destroyCookie(null, 'token');
        }
        setToken(value);
    }

    /**Dans cette fonction, on définie ce qui se passe quand on change la valeur du user */
    function handleSetUser(value: User | null) {
        if(value) {
            const valueStr = JSON.stringify(value);
            setCookie(null, 'user', valueStr) 
        } 
        setUser(value);
    }

    useEffect(() => { //Ici on détermine ce qui se passe lorsqu'on met le token, notamment comment on récupère le user
        setToken(parseCookies().token);
        if(parseCookies().user){
          const valueJSON = JSON.parse(parseCookies().user)
            setUser(valueJSON);  
        }
        
    }, []);

    return (
        <AuthContext.Provider value={{ token, setToken: handleSet, user, setUser : handleSetUser}}>
            {children}
        </AuthContext.Provider>
    );
}
