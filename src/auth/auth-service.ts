import { User } from "@/entities";
import axios from "axios";

/**Un fichier de services spécifique à l'authentification qui permet notamment d'appeler la fonction login (qui sera utilisée dans login.tsx) depuis le back*/
export async function login(email:string,password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, password});
    return response.data.token;
}

/**Idem, mais pour récupérer l'utilisateur connecté depuis le back */
export async function fetchUser() {
    const repsonse = await axios.get<User>('/api/account');
    return repsonse.data;
}

