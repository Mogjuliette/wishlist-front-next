import axios from "axios";
import router from "next/router";
import { parseCookies } from "nookies";


//On définit l'url vers laquelle axios va faire ces requêtes, stockée dans un fichier.env
axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;

/**Ici, on fait en sorte que lorsqu'il y a une requête, si on a un token, on le met dans le header de notre requête sous forme de bearer avec token */
axios.interceptors.request.use((config) => {
    const { token } = parseCookies();
    if (token) {
        config.headers.setAuthorization('Bearer ' + token);
    }
    return config;
});

/**Ici, on intercepte les requêtes qui créent une erreur 401 pour les rediriger vers la page login */
axios.interceptors.response.use((response) => response, (error) => {
    
    if (error.response?.status == 401) {
        router.push('/login');
        return Promise.reject('Unauthorized');
    }
    return Promise.reject(error);

}); 
