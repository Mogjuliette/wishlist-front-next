/**
 * Dans un fichier de services, on externalise les différentes fonctions qui font des appels vers
 * le back. On utilise notamment axios, un client HTTP basé sur des promesses.
 */

import axios from "axios";
import { Comment } from "./entities";

/**
 * Pour récupérer tous les commentaires
 * @returns Comment[]
 */
export async function fetchAllComments() {
    const response = await axios.get<Comment[]>('/api/comment');
    return response.data;
}

/**
 * Pour récupérer un commentaire avec son id
 * @returns Comment
 */
export async function fetchOneComment(id:number|string) {
    const response = await axios.get<Comment>('/api/comment/'+id);
    return response.data;
}

/**
 * Pour enregistrer un nouveau un commentaire 
 * @returns 
 */
export async function postComment(comment: Comment) {
    const response = await axios.post<Comment>('/api/comment', comment);
    return response.data;
}

/**
 * Pour modifier un commentaire 
 * @returns 
 */
export async function updateComment(comment: Comment) {
    const response = await axios.put<Comment>('/api/comment/'+comment.id, comment);
    return response.data;
}

/**
 * Pour supprimer un commentaire 
 * @returns 
 */
export async function deleteComment(id:any) {
    await axios.delete('/api/comment/'+id);
}

/**
 * Pour récupérer tous les commentaires associés à un cadeau
 * @returns Comment[]
 */
export async function fetchCommentsByGift(id:number|string) {
    const response = await axios.get<Comment[]>('/api/comment/gift/'+id);
    return response.data;
}
