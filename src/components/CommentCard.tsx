import { fetchCommentsByGift } from "@/comment-service";
import { useContext, useEffect, useState } from "react";
import { Comment } from "@/entities";
import Username from "./Username";
import { AuthContext } from "@/auth/auth-context";


interface Props {
    idGift: number;
    idUser: number; //L'id du user qui souhaite avoir le cadeau
}

/**Component permettant d'afficher un commentaire lié à un cadeau */
export default function CommentCard({idGift, idUser }: Props) {

const [comments, setComments] = useState<Comment[]>(); 
const {user} = useContext(AuthContext)

useEffect(()=>{
    fetchCommentsByGift(idGift).then( //On récupère les commentaires liés à notre cadeau
        data => setComments(data))
    
},[])
    return(
    <div>
        {/**S'il y a des commentaires et un utilisateur connecté :  */}
        {comments && comments[0] && user &&
        <div className=" mt-3">
                <ul className="list-group list-group-flush">{comments.map((commentItem)=> 
                    <div key={commentItem.id}>
                        {/**cas numéro 1 : le commentaire est accessible à tous (public) */}
                        {commentItem.isPublic && <li  className="list-group-item">
                            <div className="d-flex justify-content-between"><Username idUser={commentItem.idUser}/>
                            </div>
                            {commentItem.text}
                        </li>}
                        {/**Cas numéro 2 : le commentaire est secret et ne s'affiche pas si la personne connectée est celle qui demande ce cadeau */}
                        {!commentItem.isPublic && user.id != idUser && <li  className="list-group-item">
                            <div className="d-flex justify-content-between"><Username idUser={commentItem.idUser}/>
                            <h6><span className="badge bg-secondary">Secret</span></h6>
                            </div>
                            {commentItem.text}
                        </li>}
                    </div>)}
                </ul>
        </div>}
    </div>)
            }