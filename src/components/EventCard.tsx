import { GiftingEvent } from "@/entities";

interface Props {
    event: GiftingEvent;
}

/**Pour afficher un événement dans une liste sous forme de card */
export default function EventCard({event}:Props){
    
    return (<div className="card half mx-auto my-2">
                <div className="card-body d-flex justify-content-between align-items-center">
                    <h5 className="card-title eventName">{event.eventName}</h5>
                    <a href={"/oneEvent/"+event.id} className="btn btn-light">Voir l'événement</a>
                </div>
            </div>)
}