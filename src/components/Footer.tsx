/**Le footer du site */

export default function footer(){
    return (
        <footer>
            <nav className="navbar bg-body-tertiary">
                <div className="container-fluid d-flex justify-content-center align-items-center">
                    <span className="navbar-text">
                    @Wishlist
                    </span>
                </div>
            </nav>
        </footer>
    )
}