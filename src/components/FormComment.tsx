import { Comment } from "@/entities";
import { Router, useRouter } from "next/router";
import { FormEvent, useState } from "react";

interface Props {
    onSubmit:(comment:Comment) => void;
    edited?:Comment;
    idUser:number,
    idGift:number
}

/**Component de formulaire de cadeau, permettant d'ajouter un cadeau, utilisé dans la page addGift.tsx */
export default function FormComment({onSubmit, edited, idUser, idGift}:Props) {
    
    const router = useRouter;
    const [errors, setErrors] = useState('');
    const [comment, setComment] = useState<Comment>(edited?edited:{
        text:"",
        idUser:idUser,
        idGift:idGift,
        isPublic:false,  
    });

    function handleChange(event: any) {
        setComment({
            ...comment,
            [event.target.name]: event.target.value
        });
        console.log(comment)
    }

    function handleTrue(){
        setComment({
            ...comment,
            isPublic: true
        });
    }

    function handleFalse(){
        setComment({
            ...comment,
            isPublic: false
        });
    }


    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(comment);

        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }

    return (
    <>
                        <button type="button" className="btn btn-light mt-3" data-bs-toggle="modal" data-bs-target={"#modal"+idGift}>
                        <i className="fa-solid fa-comment"></i>
                        </button>

                        {/**Section modal permettant d'écrire un commentaire au sujet d'un cadeau dans la liste */}
                        <div className="modal fade" id={"modal"+idGift} tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                           
                            <div className="modal-body">
                                <form onSubmit={handleSubmit}>

                                    {/**Le textarea dans lequel on peut écrire le commentaire */}
                                    <div className="form-floating mb-3">
                                    <textarea className="form-control" placeholder="Ecrivez un commentaire ici" id="floatingTextarea" name="text" onChange={handleChange} value={comment.text}></textarea>
                                    <label htmlFor="floatingTextarea">Commenter</label>
                                    </div>

                                    {/**Les boutons radios pour déterminer si le commentaire est secret ou non */}
                                    <div className="form-check">
                                    <input className="form-check-input" type="radio" id="isPublicTrue" name="isPublic" value={1} onChange={handleTrue}/>
                                    <label className="form-check-label" htmlFor="isPublicTrue" >
                                        Commentaire accessible à tous
                                    </label>
                                    </div>
                                    <div className="form-check mb-3">
                                    <input className="form-check-input" type="radio" id="isPublicFalse" name="isPublic" value={0} onChange={handleFalse}/>
                                    <label className="form-check-label" htmlFor="isPublicFalse">
                                        Commentaire secret
                                    </label>
                                    </div>

                                    {/**Bouton de validation du formulaire */}
                                    <div className="d-flex justify-content-end">                                
                                    <button type="submit" className="btn btn-primary"  data-bs-dismiss="modal">Commenter</button>
                                    </div>

                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                            </div>
                            </div>
                        </div>
                        </div>
    </>)

}
