import { Gift } from "@/entities";
import { FormEvent, useState } from "react";

interface Props {
    onSubmit:(gift:Gift) => void;
    edited?:Gift;
    idUser:number,
    idEvent:number
}

/**Component de formulaire de cadeau, permettant d'ajouter un cadeau, utilisé dans la page addGift.tsx */
export default function FormGift({onSubmit, edited, idUser, idEvent}:Props) {
    
    const [errors, setErrors] = useState('');
    const [gift, setGift] = useState<Gift>(edited?edited:{
        idUserWantingGift:idUser,
        name:'',
        description:'',
        image:'',
        isOffered: false,
        idUserOfferingGift:0,
        idEvent : idEvent,
    });

    function handleChange(event: any) {
        setGift({
            ...gift,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(gift);

        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }

    return (
    <section className="bg-light p-2 p-sm-3 p-md-5">
        <form onSubmit={handleSubmit} >
            {errors && <p>{errors}</p>}

            <div className="mb-3 ">
                <label htmlFor="titre" className="form-label">Titre : </label>
                <div>
                    <div>
                        <input type="text" className="form-control half" id="name" placeholder="" name="name" value={gift.name} onChange={handleChange} required/>
                    </div>
                </div>
            </div>
            <div className="mb-3">
                <label htmlFor="description" className="form-label">Description : </label>
                <textarea className="form-control half" id="description" placeholder="" name="description" value={gift.description} onChange={handleChange} ></textarea>
            </div>
            <div className="mb-3">
                <label htmlFor="image" className="form-label">Url de l'image : </label>
                <input type="text" className="form-control half" id="image" placeholder="" name="image" value={gift.image} onChange={handleChange} />
            </div>
            
            
            <button className="btn btn-dark mt-4">Ajouter un cadeau</button>
        </form>
    </section>)

}
