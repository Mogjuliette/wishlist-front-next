import { Comment, Gift, User } from "@/entities";
import { deleteGift, deleteOfferGift, fetchAllGiftByUserAndEvent, offerGift } from "@/gift-service";
import router from "next/router";
import { useEffect, useState } from "react";
import CommentCard from "./CommentCard";
import FormComment from "./FormComment";
import { postComment } from "@/comment-service";


interface Props {
    idUser: number;
    idEvent: number;
    user : User
}

/**Une card pour récupérer depuis le back et afficher des cadeaux; sous forme d'accordéon */
export default function GiftCard({idUser, idEvent, user }: Props) {

    const [gifts, setGifts] = useState<Gift[]>();  //On définit un hook d'état pour notre liste de cadeaux

    useEffect(()=>{
        fetchAllGiftByUserAndEvent(idUser, idEvent).then(data => setGifts(data)) //On va chercher tous les cadeaux de l'utilisateur dans le back
    },[])

    /**Lorsqu'on supprime un cadeau */
    async function remove(giftId : number) {
        await deleteGift(giftId);
        router.reload();
        } 
        
    /**Lorsqu'on clique sur "je veux offrir ce cadeau" */
    async function offer(giftId : number, userId : number) {
        await offerGift(giftId, userId);
        router.reload();
        } 

    /**Lorsqu'on clique sur "Je ne veux plus offrir ce cadeau" */
    async function deleteOffer(giftId : number) {
        await deleteOfferGift(giftId);
        router.reload();
        }
    
    /**Pour poster un commentaire */
    async function addComment(comment:Comment) {
        await postComment(comment);
        router.reload();
        }

    return (<section>
        {gifts === undefined && <p className="card-text">Pas encore de cadeaux</p>}
        {gifts != undefined && !gifts[0] && <div>Pas encore de cadeaux</div>}
        {gifts && Array.isArray(gifts) && gifts.map((gift) => 
        <div key={gift.id} className="accordion " id="accordionGift">
             <div className="accordion-item m-2">
                <h2 className="accordion-header">
                <button className="accordion-button collapsed bold" type="button" data-bs-toggle="collapse" data-bs-target={"#flush"+gift.id} aria-expanded="false" aria-controls={"flush"+gift.id}>
                    {gift.name}
                </button>
                </h2>
                <div id={"flush"+gift.id} className="accordion-collapse collapse" data-bs-parent="#accordionGift">
                {gift.image && <img src={gift.image} className="square-image" alt="..." />}
                <div className="accordion-body">
                    {gift.description && <p className="pt-3">{gift.description}</p>}
                    {/**La section suivante ne s'affiche que si la colonne concernée est celle de l'utilisateur connecté */}
                    {user && user.id == idUser && 
                    <button type="button" className="btn btn-light me-3 mt-3" data-bs-toggle="tooltip" data-bs-title="Default tooltip" onClick={() =>{router.push('/oneEvent/'+gift.id+'/updateGift')}}>
                        <i className="fa-solid fa-pen"></i>
                    </button>}

                    {user && user.id == idUser && <button type="button" className="btn btn-light me-3 mt-3" onClick={() => {remove(Number(gift.id))}}>
                        <i className="fa-solid fa-trash"></i>                        
                    </button>}
                    
                    <FormComment onSubmit={addComment} idUser={Number(user.id)} idGift={Number(gift.id)} />
                    {gift.id && <CommentCard idGift={gift.id} idUser={idUser}/>}  {/**Pour afficher des commentaires */}  
                    
                    
                </div>
                    {/**La section suivante ne s'affiche que si la colonne concernée n'est pas celle de l'utilisateur connecté */}
                    {user && user.id != idUser &&<div className="card"> 
                        
                                <div className="card-body text-bg-light d-flex justify-content-center align-items-center flex-column">
                                {/**Si le cadeau n'est pas encore "offert" par un participant */}
                                {gift.isOffered == false &&<a className="giftContext" onClick={() => {offer(Number(gift.id), Number(user.id))}}>
                                Je veux offrir ce cadeau                 
                                </a>}
                                {/**Si le cadeau sera offert par un participant non connecté */}
                                {gift.isOffered && gift.idUserOfferingGift != user.id &&<p className="giftContext">Ce cadeau est déjà offert</p>}
                                {/**Si le cadeau sera offert par le participant connecté */}
                                {gift.isOffered && gift.idUserOfferingGift == user.id &&<p className="giftContext">Vous offrez ce cadeau</p>}

                                {gift.isOffered && gift.idUserOfferingGift == user.id &&<a className="giftContext" onClick={() => {deleteOffer(Number(gift.id))}}>
                                Ne plus offrir                 
                                </a>}

                                </div>
                            </div>}
                                        
                
                    </div>
                </div>  
            </div>)}
        </section>)
}
