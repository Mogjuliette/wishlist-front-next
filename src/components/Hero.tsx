/**Le hero, un beandeau promotionnel qui sera visible depuis la page d'accueil */
export default function Hero(){
    return (
        <div className="hero smallWallpaper">
            <div className="d-flex justify-content-center align-items-center black-opacity h-100">
                <div className="d-flex flex-column mx-auto p-3 half justify-content-center align-items-center">
                        <h1 className="white-h1 wishlist">Wishlist</h1>
                        <p className="white-p">L'appli efficace pour gérer en secret vos listes de Noël et d'anniversaire en groupe ! Evitez les doublons et gardez le secret du cadeau que vous allez offrir ! Idéal pour les familles et groupes d'amis !</p>
                </div>
            </div>
        </div>
    )
}