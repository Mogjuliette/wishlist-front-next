/**Un component pour faciliter l'insertion et l'affichache d'une image depuis une page */
import React from 'react';

interface ImageProps {
  src: any;
  alt: string;
  className?: string;
}

export default function ImageComponent({ src, alt, className }:ImageProps) {
  return <img src={src} alt={alt} className={className}/>;
};