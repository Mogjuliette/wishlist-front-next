import ImageComponent from "./ImageComponent";
import logoNav from "../image/logoNav.png"
import { useContext } from "react";
import { AuthContext } from "@/auth/auth-context";
import router from "next/router";


/**Barre de navigation, en haut de toutes les pages du site */
export default function navigation(){
  const { token, setToken } = useContext(AuthContext);
  
  function logout(){
    setToken(null);
    router.push('/')
  }
  
    return (     
           <header>
              <nav className="navbar navbar-expand-lg main-nav bg-body-tertiary">
                <div className="container-fluid d-flex">
                  <a className="navbar-brand ms-3" href="http://localhost:3000/"><ImageComponent src={logoNav.src} alt={"logo favicon"} className="image-nav" />
                  </a>
                  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul className="navbar-nav">
                    <li className="nav-item mx-3">
                        <a className="nav-link active" aria-current="page" href="http://localhost:3000">Accueil</a>
                      </li>
                      <li className="nav-item mx-3">
                        <a className="nav-link"  href="http://localhost:3000/account">Mes événements</a>
                      </li>
                      <li className="nav-item mx-3">
                        {!token && <a className="nav-link" href="http://localhost:3000/login">Connexion</a>}
                        {token && <a className="nav-link" onClick={() => logout()} >Déconnexion</a>}
                      </li>
                    </ul>
                  </div>
                </div>
            </nav>
          </header>
           )
}