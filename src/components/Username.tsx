import { useEffect, useState } from "react";
import { User } from "@/entities";
import { fetchOneUser } from "@/user-service";


interface Props {
    idUser: number;
}

/**Component pour récupérer le nom de l'utilisateur qui a posté un commentaire */
export default function CommentCard({idUser }: Props) {

const [user, setUser] = useState<User>(); 

useEffect(()=>{
    fetchOneUser(idUser).then(
        data => setUser(data))
    
},[])
    return(
    <div>
        {user  && 
        <strong>{user.username}</strong>}
    </div>)
            }