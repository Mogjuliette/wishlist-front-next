/**
 * Dans ce fichiers entities, on définit à quoi ressemblent les entités en typescript 
 * telles qu'elles seront récupérées depuis le back. Ici, on a quatre entités : 
 * l'utilisateur, l'événement, le cadeau et le commentaire.
 * */

export interface User {
    id?:number;
    username:string;
    email:string;
    password:string;
}

export interface GiftingEvent {
    id?:number;
    eventName:string;
}

export interface Gift {
    id?:number;
    idUserWantingGift:number;
    name:string;
    description?:string;
    image?:string;
    isOffered:boolean;
    idUserOfferingGift?:number;
    idEvent:number;
}

export interface Comment {
    id?:number;
    text:string;
    idUser:number;
    idGift:number;
    isPublic:boolean;

}