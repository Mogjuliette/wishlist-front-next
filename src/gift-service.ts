/**
 * Dans un fichier de services, on externalise les différentes fonctions qui font des appels vers
 * le back. On utilise notamment axios, un client HTTP basé sur des promesses.
 */

import axios from "axios";
import { Gift } from "./entities";

/**
 * Pour récupérer tous les cadeaux
 * @returns Gift[]
 */
export async function fetchAllGifts() {
    const response = await axios.get<Gift[]>('/api/gift');
    return response.data;
}

/**
 * Pour récupérer un cadeau avec son id
 * @returns Gift
 */
export async function fetchOneGift(id:number|string) {
    const response = await axios.get<Gift>('/api/gift/'+id);
    return response.data;
}

/**
 * Pour enregister un nouveau un cadeau
 * @returns 
 */
export async function postGift(gift: Gift) {
    const response = await axios.post<Gift>('/api/gift', gift);
    return response.data;
}

/**
 * Pour modifier un cadeau
 * @returns 
 */
export async function updateGift(gift: Gift) {
    const response = await axios.put<Gift>('/api/gift/'+gift.id, gift);
    return response.data;
}

/**
 * Pour supprimer un cadeau
 * @returns 
 */
export async function deleteGift(id:any) {
    await axios.delete('/api/gift/'+id);
}

/**
 * Pour récupérer tous les cadeaux d'un utilisateur pour un événement
 * @returns Gift[]
 */
export async function fetchAllGiftByUserAndEvent(idUser:number, idEvent:Number) {
    const response = await axios.get<Gift[]>('/api/gift/user/'+idUser+'/event/'+idEvent);
    return response.data;
}

/**
 * Pour cocher "je veux offrir ce cadeau" et changer les informations à ce sujet dans la bdd
 * @returns 
 */
export async function offerGift(giftId: number, userId: number) {
    const response = await axios.put<Gift>('/api/gift/'+giftId+'/offer/'+userId);
    return response.data;
}

/**
 * Pour cocher "je ne veux plus offrir ce cadeau" et changer les informations à ce sujet dans la bdd
 * @returns 
 */
export async function deleteOfferGift(giftId: number) {
    const response = await axios.put<Gift>('/api/gift/'+giftId+'/deleteOffer');
    return response.data;
}

