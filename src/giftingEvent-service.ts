/**
 * Dans un fichier de services, on externalise les différentes fonctions qui font des appels vers
 * le back. On utilise notamment axios, un client HTTP basé sur des promesses.
 */

import axios from "axios";
import { GiftingEvent } from "./entities";

/**
 * Pour récupérer tous les événements
 * @returns GiftingEvent[]
 */
export async function fetchAllEvents() {
    const response = await axios.get<GiftingEvent[]>('/api/giftingEvent');
    return response.data;
}

/**
 * Pour récupérer un événement en fonction de son id
 * @returns GiftingEvent
 */
export async function fetchOneEvent(id:number|string) {
    const response = await axios.get<GiftingEvent>('/api/giftingEvent/'+id);
    return response.data;
}

/**
 * Pour enregistrer un nouvel événement 
 * @returns 
 */
export async function postEvent(event: GiftingEvent) {
    const response = await axios.post<GiftingEvent>('/api/giftingEvent', event);
    return response.data;
}

/**
 * Pour modifier un événement 
 * @returns 
 */
export async function updateEvent(event: GiftingEvent) {
    const response = await axios.put<GiftingEvent>('/api/giftingEvent/'+event.id, event);
    return response.data;
}

/**
 * Pour supprimer un événement 
 * @returns 
 */
export async function deleteEvent(id:any) {
    await axios.delete('/api/giftingEvent/'+id);
}

/**
 * Pour récupérer tous les événements auquels un utilisateur participe
 * @returns 
 */
export async function fetchAllEventsByUser(id: number) {
    const response = await axios.get<GiftingEvent[]>('/api/giftingEvent/user/'+id);
    return response.data;
}