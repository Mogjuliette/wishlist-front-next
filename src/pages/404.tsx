import ImageComponent from "@/components/ImageComponent";

/**Cette page 404 permet d'avoir une page d'erreur customisée et non la page d'erreur par défaut */
export default function Custom404(){

    return(
    <div className="d-flex">
        <ImageComponent src={"https://images.unsplash.com/photo-1560270579-d515a443eb3b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1854&q=80"} alt="directions" className="w-25" />
        <div className="my-5 mx-auto">
            <h1 className="text-center my-2">Cette page n'existe pas</h1>
            <p className="text-center my-2">Erreur 404</p>
        </div>
        
    </div>)
}