import { AppProps } from "next/app";
import '../auth/axios-config';
import "@/styles/globals.css";
import "bootstrap/dist/css/bootstrap.css"
import { useEffect } from "react";
import Footer from "@/components/Footer";
import Navigation from "@/components/Navigation";
import { AuthContextProvider } from "@/auth/auth-context";


/**Dans le _app.tsx, on importe Bootstrap, ainci que les différents components dont on aura besoin dans toutes les pages */
export default function App({ Component, pageProps }: AppProps) {
    useEffect(()=> {
        import("bootstrap/dist/js/bootstrap")
    })
    return (
        <>  <AuthContextProvider>
                <Navigation />
                    <section className="main-content d-flex flex-column">
                        <Component {...pageProps} /> 
                    </section>
                <Footer /> 
            </AuthContextProvider>

        </>
    );
  }
  