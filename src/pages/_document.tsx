import { Html, Head, Main, NextScript } from 'next/document'

/**Dans le _document.tsx, on entre les différentes lignes de code qui vont dans notre balise head dans chaque page, notamment l'intégration des polices ou la favicon */
export default function Document() {
  return (
    <Html lang="en">
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
      <link href="https://fonts.googleapis.com/css2?family=Delicious+Handrawn&display=swap" rel="stylesheet" />
      <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400&display=swap" rel="stylesheet"/>
      <script src="https://kit.fontawesome.com/9d063900d6.js" crossorigin="anonymous"></script>
      <link rel="icon" type="image/png" href="https://www.bambou-diffusion.com/imgprod/SC085G.jpg" />

      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
