import { AuthContext } from "@/auth/auth-context";
import EventCard from "@/components/EventCard";
import { GiftingEvent } from "@/entities";
import { fetchAllEventsByUser } from "@/giftingEvent-service";
import { useContext, useEffect, useState } from "react";

/**Page d'un compte utilisateur dans laquelle on retrouve la liste d'événements auquel l'utilisateur participe */
export default function Events(){
 
  const [events, setEvents] = useState<GiftingEvent[]>();
  const {user} = useContext(AuthContext); // On récupère le user connecté grâce à notre AuthContext

  /**Grâce au hoook d'effet, on synchronise notre code avec un système extérieur (ici le back) le temps de récupérer le résultat de notre requête */
    useEffect(() => {
      if(!user) {
        return;
      }
      if(user.id){
        fetchAllEventsByUser(user.id).then(data => {
            setEvents(data)
        }) 
      }    
    }, [user])
  
    return(
        <div className="container-fluid d-flex justify-content-center wallpaper events">
          <div className="card half my-3">
            <div className="d-flex justify-content-center align-items-center pt-3">
              <i className="fa-solid fa-user fa-3x mt-2"></i>
            </div>
           <h1 className="text-center p-2 pb-0 user">{user?.username}</h1>
           <h2 className="text-center p-3 pt-0">Mes événements </h2>
            <div className=" align-items-start p-3">   
                {events && events[0] == undefined && <p className="text-center">Vous ne participez pas encore à un événement</p>}
                {events && events.map(item =>              
                <EventCard key={item.id} event={item}/>        
                   )} 
             </div>
            </div>
        </div>
    )
}



  