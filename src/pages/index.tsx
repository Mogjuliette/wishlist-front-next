import Logo from "../image/logo.png"
import ImageComponent from "@/components/ImageComponent";
import Hero from "@/components/Hero";

/**Page d'accueil du site, qui n'utilise pas de données du back et donc pas de services. */
export default function Home(){
    
    return(
    <>  
        <Hero />
        <div className="container-fluid"> 
                <div className="row">
                    <div className="col-md-6">
                        <div className="d-flex flex-column justify-content-center align-items-center p-sm-3 h-100">
                        <ImageComponent src={Logo.src} alt={"logo"} className="logo-size" />
                        </div>
                    </div>
                    <div className="col-md-6 d-flex justify-content-center pb-3">
                        <div className="d-flex flex-column justify-content-center align-items-start p-3 h-100 w-75">
                            <h2 className="pb-3 m-0">Pourquoi utiliser Wishlist ?</h2>
                            <p className="pb-3">Organiser vous pour offrir les cadeaux de vos listes de souhaits tout en gardant la surprise ! Une appli parfaite pour les Noëls, anniversaires, mariages et autres événements festifs.</p>
                            <a href="/account" className="btn btn-danger">Voir mes événements !</a>
                        </div>
                    </div>
                    
                </div> 
                <div className="card w-100 smallWallpaper smallConnexion d-flex justify-content-center align-items-center" >
                     <div className="card card-width my-5">
                        <div className="card-body">
                        <div className="d-flex flex-column justify-content-center align-items-center p-3 h-100">
                            <i className="fa-solid fa-user fa-3x"></i>
                            <p className="font-bold pt-3 text-center">Connectez-vous pour créer et gérer vos listes !</p>
                            <a href="/login" className="btn btn-outline-dark">Se connecter</a>
                            <p className="font-bold pt-3 text-center">Pas encore de compte ?</p>
                            <a href="/signIn" className="btn btn-outline-dark">Créer un compte</a>
                        </div>
                        </div>
                    </div>
                            
                </div>
        </div>
    </>
    )
}


 
  