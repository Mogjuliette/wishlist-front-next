import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import { User } from "@/entities";
import { fetchLoggedUser } from "@/user-service";
import { useRouter } from "next/router";
import { FormEvent, useContext, useState } from "react";

/**Page pour se connecter avec formulaire*/
export default function loginPage(){
    const router = useRouter();
    const {setToken} = useContext(AuthContext);
    const {setUser} = useContext(AuthContext);
    const [formUser, setFormUser] = useState<User>({
        email:'',
        username:'',
        password:'',
    })

    const [error, setError] = useState('');

    /**Permet de remplacer les valeur de notre useState formUser avec celles qui sont remplies au fur et à mesure par l'utilisateur */
    function handleChange(event: any) {
        setFormUser({
            ...formUser,
            [event.target.name]: event.target.value
        });
    }

    /**Gère la validation du questionnaire dans laquelle on met le token renvoyé par le back et on rentre le User connecté dans notre AuthContext. */
    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        setError('');
        try {
            setToken(await login(formUser.email, formUser.password));
            setUser(await fetchLoggedUser());
            router.push('account');
        }catch(error:any) {
            if(error.response?.status == 401) {
                setError('Invalid login/password');

            } else {
                setError('Server error');
            }
        }
    }

    return(
    <>
        <div className="d-flex align-items-center justify-content-center wallpaper connexion">
        <div className="card mx-auto my-3">
            <div className="card-body">
                <div className="d-flex flex-column justify-content-center align-items-center pt-3">
                <i className="fa-solid fa-user fa-3x mt-2"></i>                
                <h1 className="mb-3">Se connecter</h1>
                </div>
                <form onSubmit={handleSubmit}>
                <div className="mb-3">
                <label className="form-label">Email</label>
                <input type="email" className="form-control" id="email" name="email" placeholder="name@example.com" value={formUser.email} onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                <label className="form-label">Mot de passe</label>
                <input type="password" className="form-control" id="password" name="password" placeholder="mon mot de passe" value={formUser.password} onChange={handleChange} required/>
                </div>
                <div>
                    {error && <p>{error}</p>}
                <button className="btn btn-dark" type="submit">Connexion</button>
                </div>
                </form>
                <p className="mt-3">Pas encore inscrit ? <a href="/signIn">Créer votre compte</a> pour accéder à l'application !</p>
                <a className='item' onClick={() => setToken(null)}>Déconnexion</a> {/*On remet le token a null quand on se déconnecte*/}
            </div>
        </div>
        </div>
    </>)
}