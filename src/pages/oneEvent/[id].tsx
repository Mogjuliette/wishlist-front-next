import { AuthContext } from "@/auth/auth-context";
import GiftCard from "@/components/GiftCard";
import { GiftingEvent, User } from "@/entities";
import { fetchOneEvent } from "@/giftingEvent-service";
import { fetchAllUsersByEvent } from "@/user-service";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
  
/**Cette page est dédiée à un événement en particulier, avec les différentes listes de cadeaux de chaque participant. */
export default function OneEvent(){
  const router = useRouter();
  const { id } = router.query;
  const [users, setUsers] = useState<User[]>();
  const [giftingEvent, setGiftingEvent] = useState<GiftingEvent>();
  const {user} = useContext(AuthContext);
 
    useEffect(() => {
      if(!id) {
        return;
      }
        fetchAllUsersByEvent(Number(id)).then(data => { //On récupère dans un hook d'effet tous les utilisateurs d'un événements
            setUsers(data)
        });
        fetchOneEvent(Number(id)).then(data => {//On récupère aussi les informations de l'événement en lui-même
            setGiftingEvent(data)
        })     
    }, [id])

  
    return(
        <div className="container-fluid  giftingEvent p-0 height">
          <div className="d-flex align-items-center justify-content-between white-gradient wallpaper-top">
            <h1 className="p-5">{giftingEvent && giftingEvent.eventName}</h1>
            {user && <p  className="mx-5">{"Connecté.e en tant que "+user.username}</p>}
          </div>
           
            <div className="row m-0 pb-3">
            {users && users.map((itemUser)=> 
              <div key={itemUser.id} className="col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center gy-4">
                <div className="card fixed-width ">
                  <h5 className="card-header p-3">{itemUser.username}</h5>
                  <div className="card-body">
                    
                    {user?.email == itemUser.email && <button type="button" className="btn btn-light me-3 mt-3 mb-3 add"  onClick={() =>{router.push('/oneEvent/'+id+'/addGift')}}>
                        {"Ajouter un cadeau"}
                    </button>}
                    {user && <GiftCard key={itemUser.id+'gift'} idUser={Number(itemUser.id)} idEvent={Number(id)} user={user}/>}{/** Dans ce component, on récupère et affiche chaque idée de cadeau */}
                  </div>
                </div>
              </div>)}
            </div>
        </div>
    )
}


 