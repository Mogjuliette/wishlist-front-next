import { AuthContext } from "@/auth/auth-context";
import FormGift from "@/components/FormGift";
import { Gift } from "@/entities";
import { postGift } from "@/gift-service";
import { useRouter } from "next/router";
import { useContext } from "react";

/**Page de formulaire d'ajout d'un nouveau cadeau */
export default function AddGift() {
    const router = useRouter();
    const { id } = router.query;
    const {user} = useContext(AuthContext);

    async function addGift(gift:Gift) {
        await postGift(gift);
        router.push('/oneEvent/' + id);
        }

    return (
        <>
        <div className="container-fluid">
            <div className="row">
            <div className="offset-sm-2 col-sm-8 offset-md-3 col-md-6">
            <h1 className="text-center my-5">Ajouter un nouveau cadeau</h1>
            <FormGift onSubmit={addGift} idUser={Number(user?.id)} idEvent={Number(id)}/> {/**On utilise un component de formulaire d'ajout de cadeau et on lui fait passer les informations en props */}
            
            </div>
            </div>
        </div>
        </>
    );
}
