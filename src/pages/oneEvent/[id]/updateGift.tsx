import FormGiftUpdate from "@/components/FormGiftUpdate";
import { Gift } from "@/entities";
import { fetchOneGift, updateGift } from "@/gift-service";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

/** Page de formulaire permettant de modifier un cadeau existant */
export default function UpdateGift() {
    const router = useRouter();
    const { id } = router.query;
    const [gift, setGift] = useState<Gift>();

    useEffect(() => { //On récupère dans le hook d'état notre cadeau depuis le back
        if (!id) {
            return;
        }
        fetchOneGift(Number(id))
            .then(data => setGift(data))
            .catch(error => {
                console.log(error);
                if(error.response.status == 404) {
                    router.push('/404');
                }
            });
    }, [id]);


    async function update(gift:Gift) {
        await updateGift(gift);
        router.back();
        }

    return (
        <>
        <div className="container-fluid">{!gift && "erreur"}
            <div className="row">
            <div className="offset-sm-2 col-sm-8 offset-md-3 col-md-6">
            <h1 className="text-center my-5">Modifier le cadeau</h1>
            {gift && <FormGiftUpdate onSubmit={update} edited={gift}/>} {/**On utilise le component de formulaire de cadeau et on lui fait passer des props */}
            </div>
            </div>
        </div>
        </>
    );
}
