import { User } from "@/entities";
import { postUser } from "@/user-service";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";

/**Pour se créer un compte */
export default function signIn(){
    const router = useRouter();
    const [formUser, setFormUser] = useState<User>({
        email:'',
        username:'',
        password:'',
    })

    const [error, setError] = useState('');

    /**Permet de remplacer les valeur de notre useState formUser avec celles qui sont remplies au fur et à mesure par l'utilisateur */
    function handleChange(event: any) {
        setFormUser({
            ...formUser,
            [event.target.name]: event.target.value
        });
    }

    /**Gère la validation du questionnaire dans laquelle on met le token renvoyé par le back et on rentre le User connecté dans notre AuthContext. */
    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        setError('');
        try {
            await postUser(formUser);
            window.alert("Compte créé avec succès ! Veuillez à présent vous connecter avec vos identifiants")
            router.push('/login');
        }catch(error:any) {
            if(error.response?.status == 401) {
                setError('Invalid login/password');

            } else {
                setError('Server error');
            }
        }
    }

    return(
    <>
        <div className="d-flex align-items-center justify-content-center  wallpaper connexion">
        <div className="card mx-auto my-3">
            <div className="card-body">
            <div className="d-flex flex-column justify-content-center align-items-center pt-3">
                <i className="fa-solid fa-user fa-3x mt-2"></i>                
                <h1 className="mb-3">S'inscrire</h1>
                </div>
                {/**Formulaire d'inscription à proprement dit*/}
                <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label className="form-label">Nom d'utilisateur</label>
                    <input type="text" className="form-control" id="username" name="username" placeholder="Marie" value={formUser.username} onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Email</label>
                    <input type="email" className="form-control" id="email" name="email" placeholder="name@example.com" value={formUser.email} onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Mot de passe</label>
                    <input type="password" className="form-control" id="password" name="password" placeholder="mon mot de passe" value={formUser.password} onChange={handleChange} required/>
                </div>
                <div className="form-check mb-3">
                    <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" required/>
                    <label className="form-check-label">
                        En cochant cette case, j'accepte la politique de confidentialité
                    </label>
                </div>
                {error && <p>{error}</p>}
                <button className="btn btn-dark" type="submit">S'inscrire</button>
                </form>
                <p className="mt-3">Déjà inscrit ? <a href="/login">Se connecter</a> pour accéder à l'application !</p>
            </div>
        </div>
        </div>
    </>)
}