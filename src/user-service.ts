/**
 * Dans un fichier de services, on externalise les différentes fonctions qui font des appels vers
 * le back. On utilise notamment axios, un client HTTP basé sur des promesses.
 */

import axios from "axios";
import { User } from "./entities";

/**
 * Pour récupérer tous les utilisateurs
 * @returns User[]
 */
export async function fetchAllUsers() {
    const response = await axios.get<User[]>('/api/user');
    return response.data;
}

/**
 * Pour récupérer un utilisateur avec son id
 * @returns User
 */
export async function fetchOneUser(id:number|string) {
    const response = await axios.get<User>('/api/user/'+id);
    return response.data;
}

/**
 * Pour récupérer l'utilisateur qui est actuellement connecté
 * @returns User
 */
export async function fetchLoggedUser() {
    const response = await axios.get<User>('/api/protected')
    return response.data;
}

/**
 * Pour enregistrer un nouvel utilisateur
 * @returns User
 */
export async function postUser(user: User) {
    const response = await axios.post<User>('/api/user', user);
    return response.data;
}

/**
 * Pour modifier un utilisateur
 * @returns User
 */
export async function updateUser(user: User) {
    const response = await axios.put<User>('/api/user/'+user.id, user);
    return response.data;
}

/**
 * Pour supprimer un utilisateur
 * @returns User
 */
export async function deleteUser(id:any) {
    await axios.delete('/api/user/'+id);
}

/**
 * Pour récupérer tous les utilisateurs qui participent à un événement
 * @returns User
 */
export async function fetchAllUsersByEvent(id:number) {
    const response = await axios.get<User[]>('/api/user/event/'+id);
    return response.data;
}